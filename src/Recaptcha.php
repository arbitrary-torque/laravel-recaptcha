<?php

namespace ArbitraryTorque;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

/**
 * Class Recaptcha
 * @package App
 */
class Recaptcha
{
    const URI = 'https://www.google.com/recaptcha/api/siteverify';

    const ENVIRONMENT_SITE_KEY      = 'RECAPTCHA_SITE_KEY';
    const ENVIRONMENT_SECRET_KEY    = 'RECAPTCHA_SECRET_KEY';
    const ENVIRONMENT_ENABLED       = 'RECAPTCHA_ENABLED';
    const ENVIRONMENT_ERROR_MESSAGE = 'RECAPTCHA_ERROR_MESSAGE';

    const RECATCPHA_RESPONSE_KEY  = 'g-recaptcha-response';

    /**
     * @var Client
     */
    private $_client;

    /**
     * Recaptcha constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->setClient($client);
    }

    /**
     * @param Client $client
     *
     * @return Recaptcha
     */
    public function setClient(Client $client): self
    {
        $this->_client = $client;

        return $this;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        return $this->_client;
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function isValid(Request $request)
    {
        $response = $this->getClient()->post(
            self::URI,
            [
                'form_params' => [
                    'secret'   => env(self::ENVIRONMENT_SECRET_KEY),
                    'response' => $request->post(self::RECATCPHA_RESPONSE_KEY),
                    'remoteip' => $request->getClientIp()
                ]
            ]
        );

        try {
            # Account for empty response
            $response = \json_decode($response->getBody());
        } catch (\InvalidArgumentException $e) {
            return false;
        }

        return (bool)$response->success;
    }

    /**
     * @return bool
     */
    public static function enabled()
    {
        return (bool)getenv(self::ENVIRONMENT_ENABLED);
    }
}