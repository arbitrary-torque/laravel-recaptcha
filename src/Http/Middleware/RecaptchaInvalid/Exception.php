<?php

namespace ArbitraryTorque\Http\Middleware\RecaptchaInvalid;

use \Exception as BaseException;

class Exception extends BaseException
{
    const INVALID = 'Recaptcha Invalid';
}