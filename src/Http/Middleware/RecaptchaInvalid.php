<?php namespace ArbitraryTorque\Http\Middleware;

use ArbitraryTorque\Http\Middleware\RecaptchaInvalid\Exception;
use ArbitraryTorque\Recaptcha;
use Closure;

/**
 * Ensure only admin users can access various routes
 *
 * Class Data
 * @package App\Http\Middleware\Acl
 */
class RecaptchaInvalid
{
    /**
     * @var Recaptcha
     */
    private Recaptcha $recaptcha;

    /**
     * @param Recaptcha $recaptcha
     */
    public function __construct(Recaptcha $recaptcha)
    {
        $this->recaptcha = $recaptcha;
    }

    /**
     * @return Recaptcha
     */
    public function getRecaptcha(): Recaptcha
    {
        return $this->recaptcha;
    }

    /**
     * @param         $request
     * @param Closure $next
     *
     * @return mixed
     * @throws Exception
     */
    public function handle($request, Closure $next)
    {
        # Allow us to globally turn the feature on and off
        if (Recaptcha::enabled()) {
            # If we've been attached to a route, we make sure captcha is valid on every request.  We can separate
            # our routes by method, so this only applies where we need it
            if ( ! $this->getRecaptcha()->isValid($request)) {

                throw new Exception(Exception::INVALID, 400);
            }
        }

        return $next($request);
    }
}
