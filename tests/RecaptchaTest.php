<?php

namespace ArbitraryTorque\Recaptcha\Tests;

use ArbitraryTorque\Recaptcha;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Class RecaptchaTest
 * @package ArbitraryTorque\Recaptcha\Tests
 */
final class RecaptchaTest extends TestCase
{
    /**
     * @var string
     */
    private $_key = 'fhdJID63JDIii309KKJmmdki34im';
    /**
     * @var string
     */
    private $_response = 'some-made-up-thing';
    /**
     * @var string
     */
    private $_ip = '127.0.0.1';

    public function setUp() : void
    {
        parent::setUp();

        putenv('RECAPTCHA_SECRET_KEY=' . $this->getKey());
        putenv('RECAPTCHA_ENABLED=1');
    }

    /**
     * @group recaptcha
     */
    public function testIsValid()
    {
        $mockStreamInterface = $this->getMockBuilder(StreamInterface::class)->disableOriginalConstructor()->getMock();
        $mockStreamInterface->method('__toString')
            ->willReturn('{"success" : 1}');

        $mockResponse = $this->getMockBuilder(ResponseInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($mockStreamInterface);

        $recaptcha = new Recaptcha($this->getMockClient($mockResponse));

        $this->assertTrue($recaptcha->isValid($this->getMockRequest()));
    }

    /**
     * @group recaptcha
     */
    public function testIsNotValid()
    {
        $mockStreamInterface = $this->getMockBuilder(StreamInterface::class)->disableOriginalConstructor()->getMock();
        $mockStreamInterface->method('__toString')
            ->willReturn('{"success" : 0}');

        $mockResponse = $this->getMockBuilder(ResponseInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($mockStreamInterface);

        $recaptcha = new Recaptcha($this->getMockClient($mockResponse));

        $this->assertFalse($recaptcha->isValid($this->getMockRequest()));
    }

    /**
     * @group recaptcha
     */
    public function testIsNotValidEmptyResponse()
    {
        $mockStreamInterface = $this->getMockBuilder(StreamInterface::class)->disableOriginalConstructor()->getMock();
        $mockStreamInterface->method('__toString')
            ->willReturn('');

        $mockResponse = $this->getMockBuilder(ResponseInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mockResponse->expects($this->once())
            ->method('getBody')
            ->willReturn($mockStreamInterface);

        $recaptcha = new Recaptcha($this->getMockClient($mockResponse));

        $this->assertFalse($recaptcha->isValid($this->getMockRequest()));
    }

    /**
     * @group recaptcha
     */
    public function testEnabled()
    {
        $this->assertTrue(Recaptcha::enabled());
    }

    /**
     * @group recaptcha
     */
    public function testDisabled()
    {
        putenv('RECAPTCHA_ENABLED=0');

        $this->assertFalse(Recaptcha::enabled());
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->_key;
    }

    /**
     * @return string
     */
    public function getResponse()
    {
        return $this->_response;
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->_ip;
    }

    /**
     * @param $mockGuzzleResponse
     *
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    public function getMockClient($mockGuzzleResponse)
    {
        $mockClient = $this->getMockBuilder('GuzzleHttp\Client')
            ->disableOriginalConstructor()
            ->onlyMethods(['post']) # Have to do this because class is 'dynamic'
            ->getMock();

        $mockClient
            ->expects($this->once())
            ->method('post')
            ->willReturn($mockGuzzleResponse)
            ->with(
                'https://www.google.com/recaptcha/api/siteverify',
                [
                    'form_params' => [
                        'secret'   => $this->getKey(),
                        'response' => $this->getResponse(),
                        'remoteip' => $this->getIp()
                    ]
                ]
            );

        return $mockClient;
    }

    /**
     * @return \PHPUnit\Framework\MockObject\MockObject
     */
    public function getMockRequest()
    {
        $mockRequest = $this->getMockBuilder(Request::class)->disableOriginalConstructor()->getMock();

        $mockRequest
            ->expects($this->once())
            ->method('post')
            ->willReturn($this->getResponse())
            ->with(
                  'g-recaptcha-response'
            );

        $mockRequest
            ->expects($this->once())
            ->method('getClientIp')
            ->willReturn($this->getIp());

        return $mockRequest;
    }
}