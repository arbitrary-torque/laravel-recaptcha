<?php

namespace ArbitraryTorque\Recaptcha\Tests\Http\Middleware;

use ArbitraryTorque\Http\Middleware\RecaptchaInvalid;
use PHPUnit\Framework\TestCase;

/**
 * Class RecaptchaInvalidTest
 * @package ArbitraryTorque\Recaptcha\Tests\Http\Middleware
 */
class RecaptchaInvalidTest extends TestCase
{
    /**
     * @group middleware
     *        recaptcha
     */
    public function testRecaptachaValidEnabled()
    {
        putenv('RECAPTCHA_ENABLED=1');

        $requestMock = $this->getMockBuilder('Illuminate\Http\Request')->disableOriginalConstructor()->getMock();

        $recaptchaMock = $this->getMockBuilder('ArbitraryTorque\Recaptcha')
            ->disableOriginalConstructor()->getMock();

        $recaptchaMock->expects($this->once())
            ->method('isValid')
            ->with($requestMock)
            ->willReturn(true);

        $redirectorMock = $this->getMockBuilder('Illuminate\Routing\Redirector')->disableOriginalConstructor()->getMock();
        $redirectorMock->expects($this->never())
            ->method('back');

        $closure = function($request){ return $request; };

        $recaptchaInvalid = new RecaptchaInvalid($recaptchaMock, $redirectorMock);

        $this->assertEquals($requestMock, $recaptchaInvalid->handle($requestMock, $closure));
    }

    /**
     * @group middleware
     *        recaptcha
     */
    public function testRecaptachaInValidEnabled()
    {
        # Moving to exceptions and letting handler handle it
        $this->expectException(RecaptchaInvalid\Exception::class);
        $this->expectExceptionMessage(RecaptchaInvalid\Exception::INVALID);

        putenv('RECAPTCHA_ENABLED=1');

        $requestMock = $this->getMockBuilder('Illuminate\Http\Request')->disableOriginalConstructor()->getMock();

        $recaptchaMock = $this->getMockBuilder('ArbitraryTorque\Recaptcha')
            ->disableOriginalConstructor()->getMock();

        $recaptchaMock->expects($this->once())
            ->method('isValid')
            ->with($requestMock)
            ->willReturn(false);

        $closure = function($request){ return $request; };

        $recaptchaInvalid = new RecaptchaInvalid($recaptchaMock);

        $response = $recaptchaInvalid->handle($requestMock, $closure);
    }

    /**
     * @group middleware
     *        recaptcha
     */
    public function testRecaptachaInValidDisabled()
    {

        putenv('RECAPTCHA_ENABLED=0');

        $requestMock = $this->getMockBuilder('Illuminate\Http\Request')->disableOriginalConstructor()->getMock();

        $recaptchaMock = $this->getMockBuilder('ArbitraryTorque\Recaptcha')
            ->disableOriginalConstructor()->getMock();

        $recaptchaMock->expects($this->never())
            ->method('isValid')
            ->with($requestMock)
            ->willReturn(false);

        $redirectorMock = $this->getMockBuilder('Illuminate\Routing\Redirector')->disableOriginalConstructor()->getMock();
        $redirectorMock->expects($this->never())
            ->method('back');

        $closure = function($request){ return $request; };

        $recaptchaInvalid = new RecaptchaInvalid($recaptchaMock, $redirectorMock);

        $this->assertEquals($requestMock, $recaptchaInvalid->handle($requestMock, $closure));

    }

    /**
     * @group middleware
     *        recaptcha
     */
    public function testRecaptachaValidDisabled()
    {
        putenv('RECAPTCHA_ENABLED=0');

        $requestMock = $this->getMockBuilder('Illuminate\Http\Request')->disableOriginalConstructor()->getMock();

        $recaptchaMock = $this->getMockBuilder('ArbitraryTorque\Recaptcha')
            ->disableOriginalConstructor()->getMock();

        $recaptchaMock->expects($this->never())
            ->method('isValid')
            ->with($requestMock)
            ->willReturn(true);

        $redirectorMock = $this->getMockBuilder('Illuminate\Routing\Redirector')->disableOriginalConstructor()->getMock();
        $redirectorMock->expects($this->never())
            ->method('back');

        $closure = function($request){ return $request; };

        $recaptchaInvalid = new RecaptchaInvalid($recaptchaMock, $redirectorMock);

        $this->assertEquals($requestMock, $recaptchaInvalid->handle($requestMock, $closure));

    }

}