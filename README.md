Validates recaptcha

Install
```bash
composer require arbitrary-torque/laravel-recaptcha
```

Add middleware to Http\Kernal
```php
# Add middleware Http\Kernal
protected $routeMiddleware = [
    'auth'       => \Illuminate\Auth\Middleware\Authenticate::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'bindings'   => \Illuminate\Routing\Middleware\SubstituteBindings::class,
    'can'        => \Illuminate\Auth\Middleware\Authorize::class,
    'guest'      => \App\Http\Middleware\RedirectIfAuthenticated::class,
    'throttle'   => \Illuminate\Routing\Middleware\ThrottleRequests::class,
    
    'recaptcha' => \ArbitraryTorque\Recaptcha\Http\Middleware\RecaptchaInvalid::class
];
```

Add config to .env
```dotenv
RECAPTCHA_SITE_KEY=<add_key>
RECAPTCHA_SECRET_KEY=<add_key>
RECAPTCHA_ENABLED=0
RECAPTCHA_ERROR_MESSAGE="I, for one, welcome our new robot overlords!"
```

Create blade template or similar view
```blade
@if(\ArbitraryTorque\Recaptcha::enabled())

@section('js')
    @parent
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

<div class="field">
    <div class="control">
        <div class="g-recaptcha" data-sitekey="{{ env('RECAPTCHA_SITE_KEY') }}"></div>
    </div>

    @foreach ($errors->get('recaptcha') as $error)
        <p class="help is-danger">{{ $error }}</p>
    @endforeach
</div>

@endif
```